const Main = imports.ui.main
const PanelMenu = imports.ui.panelMenu
const PopupMenu = imports.ui.popupMenu
const St = imports.gi.St
const Lang = imports.lang
const Meta = imports.gi.Meta
const Shell = imports.gi.Shell
const Util = imports.misc.util

function _aboutThisMac() {
	Util.spawn(['gnome-control-center', 'info'])
}

function _softwareUpdate() {
	Util.spawn(['update-manager'])
}

function _appStore() {
	Util.spawn(['gnome-software'])
}

function _systemPreferences() {
	Util.spawn(['gnome-control-center', '--overview'])
}

function _forceQuit() {
	Util.spawn(['xkill'])
}

function _sleep() {
	Util.spawn(['systemctl', 'suspend'])
}

function _restart() {
	Util.spawn(['systemctl', 'reboot'])
}

function _shutdown() {
	Util.spawn(['systemctl', 'poweroff'])
}

function _logOut() {
	Util.spawn(['gnome-session-quit', '--no-prompt'])
}

/*
function _hover() {
	button.actor.remove_actor(icon)

	const icon_hover = new St.Icon({
		style_class: 'apple-button-hover'
	})
	
	button.actor.add_actor(icon_hover)
}
*/

const AppleButton = new Lang.Class({
    Name: "AppleButton",
    Extends: PanelMenu.Button,

    _init: function () {
        this.parent(null, "AppleButton")

        // Icon
        this.icon = new St.Icon({
            style_class: 'apple-button'
        })
        this.actor.add_actor(this.icon)

        // Menu
	this.item1 = new PopupMenu.PopupMenuItem('About This Mac')
	this.item2 = new PopupMenu.PopupSeparatorMenuItem()
	this.item3 = new PopupMenu.PopupMenuItem('Software Update...')
	this.item4 = new PopupMenu.PopupMenuItem('App Store...')
	this.item5 = new PopupMenu.PopupMenuItem('System Preferences...')
	this.item6 = new PopupMenu.PopupSeparatorMenuItem()
	this.item7 = new PopupMenu.PopupMenuItem('Force Quit...')
	this.item8 = new PopupMenu.PopupSeparatorMenuItem()
	this.item9 = new PopupMenu.PopupMenuItem('Sleep')
	this.item10 = new PopupMenu.PopupMenuItem('Restart...')
	this.item11 = new PopupMenu.PopupMenuItem('Shut Down...')
	this.item12 = new PopupMenu.PopupSeparatorMenuItem()
	this.item13 = new PopupMenu.PopupMenuItem('Log Out...')

	this.item1.connect('activate', Lang.bind(this, _aboutThisMac))
	this.item3.connect('activate', Lang.bind(this, _softwareUpdate))
	this.item4.connect('activate', Lang.bind(this, _appStore))
	this.item5.connect('activate', Lang.bind(this, _systemPreferences))
	this.item7.connect('activate', Lang.bind(this, _forceQuit))
	this.item9.connect('activate', Lang.bind(this, _sleep))
	this.item10.connect('activate', Lang.bind(this, _restart))
	this.item11.connect('activate', Lang.bind(this, _shutdown))
	this.item13.connect('activate', Lang.bind(this, _logOut))

	this.menu.addMenuItem(this.item1)
	this.menu.addMenuItem(this.item2)
	this.menu.addMenuItem(this.item3)
	this.menu.addMenuItem(this.item4)
	this.menu.addMenuItem(this.item5)
	this.menu.addMenuItem(this.item6)
	this.menu.addMenuItem(this.item7)
	this.menu.addMenuItem(this.item8)
	this.menu.addMenuItem(this.item9)
	this.menu.addMenuItem(this.item10)
	this.menu.addMenuItem(this.item11)
	this.menu.addMenuItem(this.item12)
	this.menu.addMenuItem(this.item13)
    }
})

function init() {
}
 
function enable() {
	const activitiesButton = Main.panel.statusArea['activities']
	if (activitiesButton) {
		activitiesButton.container.hide()
	}

	let indicator = new AppleButton()
	Main.panel.addToStatusArea('AppleButton', indicator, 0, 'left')

	// hide
	Main.panel.statusArea['AppleButton'].actor.visible = false

	// change icon
	//Main.panel.statusArea['AppleButton'].icon.icon_name = "appointment-soon-symbolic"

	// show
	Main.panel.statusArea['AppleButton'].actor.visible = true
}
 
function disable() {
	const activitiesButton = Main.panel.statusArea['activities']
	if (activitiesButton) {
		activitiesButton.container.show()
	}

	Main.panel.statusArea['AppleButton'].destroy()
}
